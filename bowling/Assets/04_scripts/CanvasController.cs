﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    //*****************Variables Publicas*****************
    public GameObject canvasGamePlay;
    public GameObject canvasGameOver;
    public GameObject canvasPause;
    public static bool gameover;

    void Update()
    {
        if (BallController_normal.shoots <= 0) {
            GameOver();
        }
        if (BallController_Destroy.lifes <= 0) {
            GameOver();
        }

        if (!gameover)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {   //si la velocidad es 1 que la velocidad del juego sea 0
                if (Time.timeScale == 1)
                {
                    Time.timeScale = 0;
                    canvasGamePlay.SetActive(false);
                    canvasPause.SetActive(true);
                }
                // si la velocidad es 0 que la velocidad del juego regrese a 1
                else if (Time.timeScale == 0)
                {
                    Time.timeScale = 1;
                    canvasGamePlay.SetActive(true);
                    canvasPause.SetActive(false);
                }
            }
        }
    }

    public void GameOver() {
        gameover = true;
        canvasGamePlay.SetActive(false);
        canvasGameOver.SetActive(true);
    }
    public void ButtonMenu() {
        SceneManager.LoadScene("Menu");
        
    }
    public void ButtonRestartNormal()
    {
        SceneManager.LoadScene("GamePlay");
        BallController_normal.score = 0;
        BallController_normal.shoots = 3;
        BallController_normal.speedBall = 0;
        gameover = false;
        canvasGamePlay.SetActive(true);
        canvasGameOver.SetActive(false);
        canvasPause.SetActive(false);
    }
    public void ButtonRestartCrazy()
    {
        SceneManager.LoadScene("GamePlay");
        BallController_normal.score = 0;
        BallController_normal.shoots = 3;   
        gameover = false;
        canvasGamePlay.SetActive(true);
        canvasGameOver.SetActive(false);
        canvasPause.SetActive(false);
    }
    public void ButtonRestartDestroy()
    {
        SceneManager.LoadScene("Destroy");
        BallController_Destroy.score = 0;
        BallController_Destroy.lifes = 3;
        gameover = false;
        canvasGamePlay.SetActive(true);
        canvasGameOver.SetActive(false);
        canvasPause.SetActive(false);
    }
}
