﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController_normal : MonoBehaviour {

    //*****************Variables Publicas*****************
    public GameObject puntoRestart;
    public static int speedBall;
    public static int score;
    public static int shoots = 3;
    public static bool restart = false;

    //*****************Variables Privadas*****************
    private Rigidbody rb;
    private bool lanzada = false;
    private float timeLeft = 5.1f; 
    
    void Start () {
        rb = GetComponent<Rigidbody>();
    }

	void Update () {

        if (CanvasController.gameover != true) {
            //*****************Lanzamiento de Bocha*****************
            if (Input.GetKeyDown(KeyCode.Space))
            {
                lanzada = true;
            }

            if (lanzada)
            {
                rb.AddForce(Vector3.forward * speedBall / 10);
            }

            //*****************Movimiento Lateral*****************
            Vector3 speedPosition = Vector3.left;
            if (Input.GetKey(KeyCode.LeftArrow) && lanzada == false)
            {
                transform.position += speedPosition * Time.deltaTime * 2;
            }
            if (Input.GetKey(KeyCode.RightArrow) && lanzada == false)
            {
                transform.position += -speedPosition * Time.deltaTime * 2;
            }

            //*****************Restartea a Posicion Original*****************
            rb.isKinematic = false;
            if (restart)
            {
                timeLeft -= Time.deltaTime;
                if (timeLeft < 0)
                {
                    transform.position = puntoRestart.transform.position;
                    rb.isKinematic = true;
                    lanzada = false;
                    restart = false;
                    shoots = shoots - 1;
                    speedBall = 0;
                    CanvasGamePlay.powerDecide = false;
                    CanvasGamePlay.timeLeftPower = 5.1f;
                   
                }
            }
        }
    }

    private void OnCollisionEnter(Collision col)
    {

        if (col.gameObject.tag == "restart_pos_ball")
        {
            restart = true;
        }
    }
}
