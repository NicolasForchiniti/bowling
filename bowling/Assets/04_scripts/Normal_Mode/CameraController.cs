﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject ball;
    private Vector3 offset;

	// Use this for initialization
	void Start () {
        offset = ball.transform.position - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
       transform.position = ball.transform.position - offset;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "stopCamara") {
            Debug.Log("hola");
            Destroy(gameObject);
        }
    }
}
