﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CanvasGamePlay : MonoBehaviour
{

    //*****************Variables Publicas*****************
    public Text scoreText;
    public Text shootsText;
    public Text powerText;
    public static float timeLeftPower = 5.1f;
    public static bool powerDecide = false;
    public static bool gameOver = false;

    void Update()
    {
        //*****************Asignacion de valor a los Textos*****************
        scoreText.text = "Score: " + BallController_normal.score;
        shootsText.text = "Shoot: " + BallController_normal.shoots;
        powerText.text = "Power:" + BallController_normal.speedBall / 10;

        //*****************Power en Ascendente y Descendiente*****************
        if (!powerDecide)
        {
            timeLeftPower -= Time.deltaTime;
            if (timeLeftPower > 0.0f)
            {
                BallController_normal.speedBall = BallController_normal.speedBall + 1;
            }
            else
            {
                BallController_normal.speedBall = BallController_normal.speedBall - 1;
            }
            if (timeLeftPower <= -5.1f)
            {
                timeLeftPower = 5.1f;
            }
            if (Input.GetKey(KeyCode.Space))
            {
                powerDecide = true;
            }
        }
    }
}
