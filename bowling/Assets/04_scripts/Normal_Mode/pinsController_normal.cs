﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pinsController_normal : MonoBehaviour
{
    private bool fall = false;
    private float timeLeft;
    // Use this for initialization
    void Start()
    {
        timeLeft = 3.0f;
    }


    // Update is called once per frame
    void Update()
    {
        if (transform.localEulerAngles.x < -40f || transform.localEulerAngles.x < 40f && transform.localEulerAngles.z < -26f || transform.localEulerAngles.z > 26f && !fall)
        {
            BallController_normal.score += 10;
            fall = true;
        }
        if (fall)
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
            {
                Destroy(gameObject);
                timeLeft = 3.0f;
            }
        }

    }
}
