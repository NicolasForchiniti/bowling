﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinesController : MonoBehaviour
{

    public GameObject[] pins;
    public Vector3 spawnValues;
    public float spawnWait;
    public float spawnMostWait;
    public float spawnLeastWait;  
    public int startWait;
    public bool stop;

    int randEnemy;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(waitSpawner());
    }

    // Update is called once per frame
    void Update()
    {
        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
        if (BallController_Destroy.lifes == 0) {
            stop = true;
            StopCoroutine(waitSpawner());
        }
        if (BallController_Destroy.score == 100) {
            spawnMostWait = 2;
            spawnLeastWait = 1;
        }
        if (BallController_Destroy.score == 500)
        {
            spawnMostWait = 2;
            spawnLeastWait = 0.5f;
        }
        if (BallController_Destroy.score == 900)
        {
            spawnMostWait = 1;
            spawnLeastWait = 0.5f;
        }
    }

    IEnumerator waitSpawner() {

        yield return new WaitForSeconds(startWait);

        while (!stop) {
            randEnemy = Random.Range(0, 1);
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x),1, Random.Range(-spawnValues.z, spawnValues.z));
            Instantiate(pins[randEnemy], spawnPosition + transform.TransformPoint(0, 0, 0), gameObject.transform.rotation);
            yield return new WaitForSeconds(spawnWait);
        }
    }
}
