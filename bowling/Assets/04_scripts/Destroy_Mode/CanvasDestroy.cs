﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CanvasDestroy : MonoBehaviour {

    //*****************Variables Publicas*****************
    public Text scoreText;
    public Text lifesText;

    void Update()
    {
        //*****************Asignacion de valor a los Textos*****************
        scoreText.text = "Score: " + BallController_Destroy.score;
        lifesText.text = "Lifes: " + BallController_Destroy.lifes;
    }
}
