﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CanvasGameOverDestroy : MonoBehaviour {
    //*****************Variables Publicas*****************
    public Text scoreText;
	
	void Update () {
        
        //*****************Asignacion de valor a los Textos*****************
        scoreText.text = "Score: " + BallController_Destroy.score;
    }
}
