﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController_Destroy : MonoBehaviour
{

    //*****************Variables Publicas*****************
    public GameObject puntoRestart;
    public static int speedBall = 30;
    public static int score;
    public static int lifes = 3;
    //*****************Variables Privadas*****************
    private Rigidbody rb;
    private bool restart = false;
    private bool lanzada = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {

        if (CanvasController.gameover != true)
        {
            //*****************Lanzamiento de Bocha*****************
            if (Input.GetKeyDown(KeyCode.Space))
            {
                lanzada = true;
            }

            if (lanzada)
            {
                rb.AddForce(Vector3.forward * speedBall);
            }

            //*****************Movimiento Lateral*****************
            Vector3 speedPosition = Vector3.left;
            if (Input.GetKey(KeyCode.LeftArrow) && lanzada == false)
            {
                transform.position += speedPosition * Time.deltaTime * 2;
            }
            if (Input.GetKey(KeyCode.RightArrow) && lanzada == false)
            {
                transform.position += -speedPosition * Time.deltaTime * 2;
            }

            //*****************Restartea a Posicion Original*****************
            rb.isKinematic = false;
            if (restart)
            {
                transform.position = puntoRestart.transform.position;
                rb.isKinematic = true;
                lanzada = false;
                restart = false;     
            }
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "pin")
        {
            Destroy(col.gameObject);
            score = score + 10;
        }

        if (col.gameObject.tag == "restart_pos_ball")
        {
            restart = true;
        }
    }
}
