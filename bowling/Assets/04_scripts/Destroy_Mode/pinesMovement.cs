﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pinesMovement : MonoBehaviour {

    public float speedPin;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 movimiento = Vector3.forward;

        transform.position -= movimiento * speedPin * Time.deltaTime;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "destroy_pins") {
            Destroy(gameObject);
            BallController_Destroy.lifes--;
        }
    }
}
