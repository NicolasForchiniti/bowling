﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class MenuController : MonoBehaviour {

    public GameObject canvasMode;
    public GameObject canvasMenu;
    public GameObject canvasCredits;

    public void selectModeNormal()
    {
        SceneManager.LoadScene("GamePlay");
    }
    public void selectModeDestroy()
    {
        SceneManager.LoadScene("Destroy");
    }
    public void selectModeCrazyl()
    {
        SceneManager.LoadScene("Destroy");
    }
    public void selectMode()
    {
        canvasMode.SetActive(true);
        canvasMenu.SetActive(false);
        canvasCredits.SetActive(false);
    }
    public void selectCredits()
    {
        canvasMode.SetActive(false);
        canvasMenu.SetActive(false);
        canvasCredits.SetActive(true);
    }
    public void selectMenu()
    {
        canvasMode.SetActive(false);
        canvasMenu.SetActive(true);
        canvasCredits.SetActive(false);
    }
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {

    }
}
